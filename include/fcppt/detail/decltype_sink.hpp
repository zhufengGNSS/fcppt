//          Copyright Carl Philipp Reh 2009 - 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)


#ifndef FCPPT_DETAIL_DECLTYPE_SINK_HPP_INCLUDED
#define FCPPT_DETAIL_DECLTYPE_SINK_HPP_INCLUDED


namespace fcppt
{
namespace detail
{

template<
	typename Type
>
Type
decltype_sink(
	Type const &
);

}
}

#endif
