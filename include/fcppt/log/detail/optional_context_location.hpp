//          Copyright Carl Philipp Reh 2009 - 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)


#ifndef FCPPT_LOG_DETAIL_OPTIONAL_CONTEXT_LOCATION_HPP_INCLUDED
#define FCPPT_LOG_DETAIL_OPTIONAL_CONTEXT_LOCATION_HPP_INCLUDED

#include <fcppt/log/context_location.hpp>
#include <fcppt/log/detail/optional_context_location_fwd.hpp>
#include <fcppt/optional/object_impl.hpp>


#endif
