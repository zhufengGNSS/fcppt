//          Copyright Carl Philipp Reh 2009 - 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)


#ifndef FCPPT_LOG_OPTIONAL_LOCATION_HPP_INCLUDED
#define FCPPT_LOG_OPTIONAL_LOCATION_HPP_INCLUDED

#include <fcppt/log/location.hpp>
#include <fcppt/log/optional_location_fwd.hpp>
#include <fcppt/optional/object_impl.hpp>


#endif
