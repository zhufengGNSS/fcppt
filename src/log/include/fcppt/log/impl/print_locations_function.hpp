//          Copyright Carl Philipp Reh 2009 - 2016.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)


#ifndef FCPPT_LOG_IMPL_PRINT_LOCATIONS_FUNCTION_HPP_INCLUDED
#define FCPPT_LOG_IMPL_PRINT_LOCATIONS_FUNCTION_HPP_INCLUDED

#include <fcppt/io/ostream.hpp>
#include <fcppt/log/tree_function.hpp>


namespace fcppt
{
namespace log
{
namespace impl
{

fcppt::log::tree_function
print_locations_function(
	fcppt::io::ostream &
);

}
}
}

#endif
